# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 14:20:07 2020

@author: Madou
"""

import mysql.connector
from mysql.connector import Error

def updateLaptopPrice(id, price):
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='electronics',
                                             user='root',
                                             password='')

        cursor = connection.cursor()
        sql_update_query = """Update laptop set price = %s where id = %s"""
        inputData = (price, id)
        cursor.execute(sql_update_query, inputData)
        connection.commit()
        print("Record Updated successfully ")

    except mysql.connector.Error as error:
        print("Failed to update record to database: {}".format(error))
    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")

updateLaptopPrice(7500, 1)
updateLaptopPrice(5000, 2)