# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 13:11:16 2020

@author: Madou
"""

import mysql.connector
from mysql.connector import Error

def getLaptopDetail(id):
    try:
        mySQLConnection = mysql.connector.connect(host='localhost',
                                                  database='electronics',
                                                  user='root',
                                                  password='')

        cursor = mySQLConnection.cursor(buffered=True)
        sql_select_query = """select * from laptop where id = %s"""
        cursor.execute(sql_select_query, (id,))
        record = cursor.fetchall()

        for row in record:
            print("Id = ", row[0], )
            print("Name = ", row[1])
            print("Join Date = ", row[2])
            print("Salary  = ", row[3], "\n")

    except mysql.connector.Error as error:
        print("Failed to get record from MySQL table: {}".format(error))

    finally:
        if (mySQLConnection.is_connected()):
            cursor.close()
            mySQLConnection.close()
            print("MySQL connection is closed")

id1 = 1
id2 = 2
getLaptopDetail(id1)
getLaptopDetail(id2)