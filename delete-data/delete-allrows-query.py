# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 14:42:28 2020

@author: Madou
"""

import mysql.connector
from mysql.connector import Error

try:
    connection = mysql.connector.connect(host='localhost',
                                         database='electronics',
                                         user='root',
                                         password='')
    cursor = connection.cursor()
    Delete_all_rows = """truncate table Laptop """
    cursor.execute(Delete_all_rows)
    connection.commit()
    print("All Record Deleted successfully ")

except mysql.connector.Error as error:
    print("Failed to Delete all records from database table: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")